import { describe, expect, it, test } from 'vitest'
import { RegisterUserService } from './register'
import { compare } from 'bcrypt'
import { InMemoryUsersRepository } from '@/repositories/in-memory/in-memory-users-repository'
import { UserAlreadyExistsError } from './errors/userAlreadyExists'

describe('Register Use Case', () => {
   it('should be able to register', async () => {
      const usersRepository = new InMemoryUsersRepository()
      const registerUserService = new RegisterUserService(usersRepository)
      const { user } = await registerUserService.execute({
         name: 'Rafael',
         email: 'rafinha@icloud.com',
         password: '123456',
      })

      expect(user.id).toEqual(expect.any(String))
   })

   it('should hash user password upon registration', async () => {
      const usersRepository = new InMemoryUsersRepository()
      const registerUserService = new RegisterUserService(usersRepository)
      const { user } = await registerUserService.execute({
         name: 'Rafael',
         email: 'rafinha@icloud.com',
         password: '123456',
      })
      const isPawwordCorrectlyHashed = await compare(
         '123456',
         user.password_hash,
      )
      expect(isPawwordCorrectlyHashed).toBe(true)
   })

   it('should not able to register with same email twice', async () => {
      const usersRepository = new InMemoryUsersRepository()
      const registerUserService = new RegisterUserService(usersRepository)
      const email = 'rafa_10@icloud.com'
      await registerUserService.execute({
         name: 'Rafael',
         email,
         password: '123456',
      })
      expect(() =>
         registerUserService.execute({
            name: 'Rafael',
            email,
            password: '123456',
         }),
      ).rejects.toBeInstanceOf(UserAlreadyExistsError)
   })
})
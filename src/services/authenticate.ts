import { UserRepository } from "@/repositories/users-repository";
import { InvalidCredentialsError } from "./errors/invalid-credentials-error";
import { compare } from "bcrypt";
import { User } from "@prisma/client";

interface AuthenticateServiceRequest {
   email: string
   password: string
}
interface AuthenticateServiceResponse {
   user: User
}

export class AuthenticateService {
   constructor(private userRepositoriy: UserRepository) { }

   async execute({ email,
      password,
   }: AuthenticateServiceRequest): Promise<AuthenticateServiceResponse> {
      const user = await this.userRepositoriy.findByEmail(email)
      if (!user) {
         throw new InvalidCredentialsError()
      }
      const doesPasswordMatches = await compare(password, user.password_hash)
      if (!doesPasswordMatches) {
         throw new InvalidCredentialsError()
      }
      return {
         user,
      }
   }
}
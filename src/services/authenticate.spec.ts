import { describe, expect, it } from 'vitest'
import { InMemoryUsersRepository } from '@/repositories/in-memory/in-memory-users-repository'
import { AuthenticateService } from './authenticate'
import { hash } from 'bcrypt'
import { InvalidCredentialsError } from './errors/invalid-credentials-error'

describe('Register Use Case', () => {
   it('should be able to register', async () => {
      const usersRepository = new InMemoryUsersRepository()
      const sut = new AuthenticateService(usersRepository)

      expect(() => sut.execute({
         email: 'rafinha@icloud.com',
         password: '123456',
      })).rejects.toBeInstanceOf(InvalidCredentialsError)
   })

   it('should not  be able to authenticate with wrong email', async () => {
      const usersRepository = new InMemoryUsersRepository()
      const sut = new AuthenticateService(usersRepository)
      await usersRepository.create({
         name: 'Rafael',
         email: 'rafinha@icloud.com',
         password_hash: await hash('123456', 6)
      })
      const { user } = await sut.execute({
         email: 'rafinha@icloud.com',
         password: '123456',
      })

      expect(user.id).toEqual(expect.any(String))
   })


    it('should not  be able to authenticate with wrong password', async () => {
      const usersRepository = new InMemoryUsersRepository()
      const sut = new AuthenticateService(usersRepository)
      await usersRepository.create({
         name: 'Rafael',
         email: 'rafinha@icloud.com',
         password_hash: await hash('123456', 6)
      })
      const { user } = await sut.execute({
         email: 'rafinha@icloud.com',
         password: '123456',
      })

      expect(user.id).toEqual(expect.any(String))
   })


})
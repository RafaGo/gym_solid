import { UserRepository } from "@/repositories/users-repository"
import { hash } from "bcrypt"
import { UserAlreadyExistsError } from "./errors/userAlreadyExists"
import { User } from "@prisma/client"

interface RegisterUser {
   name: string
   email: string
   password: string
}

interface RegisterUseCaseResponse {
   user: User

}

export class RegisterUserService {
   constructor(private usersRepository: UserRepository) {
   }
   async execute({ name, email, password }: RegisterUser): Promise<RegisterUseCaseResponse> {
      const password_hash = await hash(password, 6)
      const userWhitSameEmail = await this.usersRepository.findByEmail(email)
      if (userWhitSameEmail) {
         throw new UserAlreadyExistsError()
      }
      const user = await this.usersRepository.create({
         name,
         email,
         password_hash
      })
      return {
         user
      }
   }
}

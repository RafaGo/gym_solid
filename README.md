# App 

GymPass style app. 

## RFs (Requisitos funcionais)

- [x] Deve ser possivel se cadastrar. 
- [ ] Deve ser possivel se autenticar. 
- [ ] Deve ser possivel obter o perfil de um usuario logado.
- [ ] Deve ser possivel obter o número de check-ins realizados pelo usuario.
- [ ] Deve ser possivel o usuario obter seu histórico de check-ins. 
- [ ] Deve ser possivel o usuario buscar academias próximas . 
- [ ] Deve ser possivel o usuario buscar academias pelo nome . 
- [ ] Deve ser possivel o usuario realizar check-in em uma academia  . 
- [ ] Deve ser possivel validar o check-in de um usuario  . 
- [ ] Deve ser possivel cadastrar academia  . 

## RNs (Regras de negócio)

- [x] Usuario não deve poder se cadastrar com email duplicado . 
- [ ] Usuario não deve fazer check-in no mesmo dia . 
- [ ] Usuario não deve fazer check-in se não estiver perto (100m) da academia . 
- [ ] O  check-in só pode ser validado até 20 minutos após criado .
- [ ] O check-in só pode ser validado por administradores . 

## RNFs (Requisitos não-funcionais)

- [x] A senha do usuario precisa estar criptografada. 
- [ ] Os dados da aplicação precisam estar persistidos em um banco PostgreSQL.
- [ ] Todas listas de dados precisam paginadas com 20 itens por pagina . 
- [ ] O Usuario deve ser identificado por um JWT (JSON Web Token) . 






